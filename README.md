# Step Project 3 Cards 
## by Nataly Vorobei, Eugene Emets 
### DAN IT FE 14 Online Advanced JS Course

Created by: 

[Nataly Vorobei](https://gitlab.com/NatVorobei), 

[Eugene Emets](https://gitlab.com/expero1)
---

[Repository](https://gitlab.com/expero1/step-project-3-cards)

[GitLab Pages](https://expero1.gitlab.io/step-project-3-cards/)
---

## Technologies used:

### The page uses:

- HTML
- CSS
- JavaScript

### Project build Tools:

- Gulp
- SCSS

  
### Additional Tools:
- git
- node
- npm
- gitlab
- github pages
---
## Completed tasks:

Nataly Vorobei:

- Header Section
- Edit visit Form
- Styles for whole page

Eugene Emets:

- API and local state code
- Filter section
- Visit Cards Section
