import { login } from "../src/js/controller/controller.js";
import { email, password, token as testToken } from "../test-context.js";
import { jest } from "@jest/globals";
describe("Test API", () => {
  test("login test"),
    async () => {
      expect.assertions(1);
      const recievedToken = await login(email, password);
      recievedToken.toBe(testToken);
    };
});
