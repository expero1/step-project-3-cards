import {
  showCardsSection,
  toggleFiltersVisibility,
  showHeader,
} from "./controller/render-sections.js";
import { registerErrorEventsListeners } from "./error/app-error.js";
registerErrorEventsListeners();
showHeader();
toggleFiltersVisibility();
showCardsSection();
