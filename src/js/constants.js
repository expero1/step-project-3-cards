export const doctors = {
  cardiologist: "Cardiologist",
  dentist: "Dentist",
  therapist: "Therapist",
};

export const urgency = {
  urgent: "High",
  common: "Low",
  important: "Normal",
};

export const status = {
  open: "Open",
  done: "Done",
};
