import { doctors, urgency, status } from "../constants.js";
import { AppError, errorMessages } from "../error/app-error.js";
import {
  createCardiologisVisit,
  createDentistVisit,
  createTherapistVisit,
} from "./create-visit-information.js";
const reqiredParameter = (message, key, value = undefined) => {
  return { false: { message, key, value } };
};
const isNumber = (value) =>
  (value !== "" && typeof Number(value) === "number") || "Value must be number";
const notEmpty = (value) =>
  (value && value.length != 0) || `Field must not be empty`;
const inRange = (minRange, maxRange) => (value) =>
  (value >= minRange && value <= maxRange) ||
  `Value must be in range of ${minRange} and ${maxRange}`;
const allowableValues =
  (...allowValues) =>
  (value) =>
    allowValues.includes(value) ||
    `Value must be in one of ${allowValues.join(", ")}`;
const everyValue =
  (...functions) =>
  (value) => {
    Array.isArray(value) || (value = [value]);
    const result = [];
    value.forEach((currentValue) => {
      functions.forEach((func) => {
        const checkValue = func(currentValue);
        checkValue === true ||
          result.includes(checkValue) ||
          result.push(checkValue);
      });
    });
    return result.length > 0 ? result[0] : true;
  };
const checkFunctions = {
  doctor: [
    allowableValues(doctors.cardiologist, doctors.dentist, doctors.therapist),
  ],
  visitPurpose: [notEmpty],
  visitDescription: [notEmpty],
  visitUrgency: [
    allowableValues(urgency.urgent, urgency.important, urgency.common),
  ],
  patientName: [notEmpty],
  patientSystolicPressure: [isNumber, notEmpty, inRange(50, 200)],
  patientDiastolicPressure: [isNumber, notEmpty, inRange(50, 200)],
  patientBodyMassIndex: [isNumber, notEmpty],
  patientPastIllness: [everyValue(notEmpty)],
  patientAge: [isNumber, notEmpty, inRange(0, 120)],
  lastVisitDate: [],
  visitStatus: [
    allowableValues(status.open, status.done)
  ]
};

const checkParameter = (fieldName, value) => {
  const checkResult = [];
  if (!(fieldName in checkFunctions))
    throw new AppError(`FieldName ${fieldName} have not checked`);
  for (let func of checkFunctions[fieldName]) {
    const check = func(value);
    if (check !== true) checkResult.push(check);
  }

  return checkResult;
};

export const checkForm = (formData) => {
  let checkResult = {};
  switch (formData.doctor) {
    case doctors.cardiologist:
      formData = createCardiologisVisit(formData);
      break;
    case doctors.dentist:
      formData = createDentistVisit(formData);
      break;
    case doctors.therapist:
      formData = createTherapistVisit(formData);
      break;
    default:
      throw new AppError(errorMessages.visitStatusError, formData);
  }
  for (let key in formData) {
    const fieldcheckResult = checkParameter(key, formData[key]);
    if (fieldcheckResult.length > 0) checkResult[key] = fieldcheckResult;
  }
  console.log(checkResult);
  return checkResult;
};
