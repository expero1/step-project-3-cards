import { State } from "../model/state.js";
import {
  getVisits as apiGetVisits,
  setVisit as apiSetVisit,
  updateVisit as apiUpdateVisit,
  deleteVisit as apiDeleteVisit,
  getToken as apiGetToken,
} from "../model/api.js";
import {
  AppError,
  checkCardNotExistError,
  checkLoginError,
  checkTokenError,
  errorMessages,
} from "../error/app-error.js";
import { doctors } from "../constants.js";
import { CustomEvents } from "../events/events.js";
export const loginStatusChanged = () => {
  CustomEvents.sendEvent(CustomEvents.eventNames.userLoggedInStatusChanged);
};

export const logout = () => {
  State.removeToken();
  State.clearState();
  loginStatusChanged();
  CustomEvents.sendEvent(CustomEvents.eventNames.logout);
};
const logoutOnTokenError = (error) => {
  try {
    checkTokenError(error);
  } catch (error) {
    logout();
    throw error;
  }
};
export const login = async (email, password) => {
  try {
    const token = await apiGetToken(email, password);
    State.setToken(token);
    loginStatusChanged();
    CustomEvents.sendEvent(CustomEvents.eventNames.login);
    await getVisits();
  } catch (error) {
    checkLoginError(error);
    throw error;
  }
};

export const isLoggedIn = () => {
  return State.getToken() ? true : false;
};

export const getVisits = async () => {
  if (!isLoggedIn) throw new AppError(errorMessages.loginError);
  const askVisits = State.isAskVisits();
  try {
    if (!askVisits) {
      const visits = apiGetVisits(State.getToken());
      State.setVisits(visits);
      const visitsFromServer = await visits;
      State.setVisits(visitsFromServer);
    }
    return (await State.getVisits()).sort(
      (firstVisitCard, secondVisitCard) =>
        firstVisitCard.id - secondVisitCard.id
    );
  } catch (error) {
    logoutOnTokenError(error);
    throw error;
  }
};

export const getVisit = async (cardId) => {
  if (!State.isAskVisits()) await getVisits();

  const visitInformation = State.getVisit(cardId);
  if (!visitInformation) throw new AppError(errorMessages.cardNotExists);
  return visitInformation;
};

export const setVisit = async (visitInformation) => {
  if (!isLoggedIn()) throw new AppError(errorMessages.loginError);
  const token = State.getToken();

  try {
    const savedVisitInformation = await apiSetVisit(token, visitInformation);
    State.setVisit(savedVisitInformation.id, savedVisitInformation);
    CustomEvents.sendEvent(
      CustomEvents.eventNames.addCard,
      State.getVisit(savedVisitInformation.id)
    );
    return State.getVisit(savedVisitInformation.id);
  } catch (error) {
    logoutOnTokenError(error);
    throw error;
  }
};
export const updateVisit = async (cardId, visitInformation) => {
  const token = State.getToken();
  const savedVisitInformation = await apiUpdateVisit(
    token,
    cardId,
    visitInformation
  ).catch((error) => {
    logoutOnTokenError(error);
    checkCardNotExistError(cardId, error);
    throw error;
  });
  // State.updateVisit(savedVisitInformation.id, savedVisitInformation);
  State.deleteVisit(savedVisitInformation.id);
  State.setVisit(savedVisitInformation.id, savedVisitInformation);

  CustomEvents.sendEvent(
    CustomEvents.eventNames.updateCard,
    State.getVisit(savedVisitInformation.id)
  );
  return State.getVisit(savedVisitInformation.id);
};
export const deleteVisit = async (cardId) => {
  const token = State.getToken();
  try {
    await apiDeleteVisit(token, cardId);
    State.deleteVisit(cardId);
  } catch (error) {
    logoutOnTokenError(error);
    checkCardNotExistError(cardId, error);
    throw error;
  }
};

export const saveFormToState = (formData) => {
  typeof formData?.patientPastIllness === "string" &&
    (formData.patientPastIllness = [formData.patientPastIllness]);
  State.saveFormToState(formData);
};
export const getFormFromState = () => {
  return State.getFormFromState();
};
export const clearFormInState = () => {
  State.clearFormInState();
};
export const saveFormErrorsToState = (formData) => {
  State.saveFormErrorsToState(formData);
};
export const getFormErrorsFromState = () => {
  return State.getFormErrorsFromState();
};
export const clearFormErrorsInState = () => {
  State.clearFormErrorsInState();
};
