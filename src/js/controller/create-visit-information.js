import { doctors } from "../constants.js";

export const createCardiologisVisit = function ({
  visitPurpose,
  visitDescription,
  visitUrgency,
  patientName,
  patientSystolicPressure,
  patientDiastolicPressure,
  patientBodyMassIndex,
  patientPastIllness = [],
  patientAge,
}) {
  return {
    visitPurpose,
    visitDescription,
    visitUrgency,
    patientName,
    patientSystolicPressure,
    patientDiastolicPressure,
    patientBodyMassIndex,
    patientPastIllness,
    patientAge,
    doctor: doctors.cardiologist,
  };
};
export const createDentistVisit = function ({
  visitPurpose,
  visitDescription,
  visitUrgency,
  patientName,
  lastVisitDate = [],
}) {
  return {
    visitPurpose,
    visitDescription,
    visitUrgency,
    patientName,
    lastVisitDate,
    doctor: doctors.dentist,
  };
};
export const createTherapistVisit = function ({
  visitPurpose,
  visitDescription,
  visitUrgency,
  patientName,

  patientAge,
}) {
  return {
    visitPurpose,
    visitDescription,
    visitUrgency,
    patientName,
    patientAge,
    doctor: doctors.therapist,
  };
};
