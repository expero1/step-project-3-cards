export const filterVisits = (visits, filterInformation) => {
  const filteredVisits = visits.filter((visit) => {
    return (
      filterByContent(visit, filterInformation.contentSearch) &&
      filterByStatus(visit, filterInformation.visitStatus) &&
      filterByUrgency(visit, filterInformation.visitUrgency)
    );
  });
  return filteredVisits;
};

const filterByContent = (visit, filterText) => {
  const filteredFields = [
    "patientName",
    "visitDescription",
    "visitPurpose",
    "patientPastIllness",
    "doctor",
  ];
  if (filterText.length === 0) return true;
  return filteredFields.some((fieldName) =>
    Object.keys(visit).includes(fieldName)
      ? visit[fieldName].toLowerCase().includes(filterText.toLowerCase())
      : false
  );
};
const filterByStatus = (visit, statuses) => {
  if (statuses === undefined) return true;
  if (!Array.isArray(statuses)) statuses = [statuses];
  return statuses.includes(visit.visitStatus);
};

export const filterByUrgency = (visit, visitUrgency) => {
  if (visitUrgency === undefined) return true;
  if (!Array.isArray(visitUrgency)) visitUrgency = [visitUrgency];
  return visitUrgency.includes(visit.visitUrgency);
};
