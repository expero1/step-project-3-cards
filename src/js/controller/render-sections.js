import { handleError } from "../error/app-error.js";
import { CustomEvents } from "../events/events.js";
import { FilterSection } from "../html-element/filter/filter.js";
import { VisitCardsSection } from "../html-element/cards/cards-section.js";
import { createVisitCard } from "../html-element/cards/create-visit-card.js";
import { Header } from "../html-element/parts/header.js";
import { getVisits, isLoggedIn } from "./controller.js";

const visitCardSection = document.querySelector(".visit-cards-section");

const getVisitCardById = (visitCardId) =>
  visitCardSection.querySelector(`[data-id="${visitCardId}"]`);

const allVisibleVisitCards = () =>
  visitCardSection.querySelectorAll(".visit-card:not(.hidden)");

const noCardsMessage = () => visitCardSection.querySelector(".no-visits");

export const showCardsSection = async (visitCards) => {
  const visitCardsSection = document.querySelector(".visit-cards-section");
  visitCardsSection.innerHTML = "";
  if (isLoggedIn()) {
    // try {
    if (!visitCards) visitCards = await getVisits();
    visitCardsSection.append(new VisitCardsSection(visitCards).rendered);
    // } catch (error) {
    //   handleError(error);
    // }
  } else visitCardsSection.innerHTML = `<p class="info">Please login</p>`;
};

export const showHeader = () => {
  const header = new Header();
  const documentHeader = document.body.querySelector("header");
  documentHeader.append(header.rendered);
};

export const toggleFiltersVisibility = () => {
  const filterSection = document.body.querySelector(".filter-section");
  filterSection.innerHTML = "";
  isLoggedIn() && filterSection.append(new FilterSection().rendered);
};

document.addEventListener(CustomEvents.eventNames.logout, async () => {
  showCardsSection();
  toggleFiltersVisibility();
});

document.addEventListener(CustomEvents.eventNames.login, async () => {
  showCardsSection(await getVisits());
  toggleFiltersVisibility();
  CustomEvents.sendEvent(CustomEvents.eventNames.reloadCards);
});

document.addEventListener(CustomEvents.eventNames.addCard, async (event) => {
  const cardInformation = event.detail;
  visitCardSection.prepend(createVisitCard(cardInformation).rendered);
  CustomEvents.sendEvent(CustomEvents.eventNames.reloadCards);
});

document.addEventListener(CustomEvents.eventNames.updateCard, async (event) => {
  const cardInformation = event.detail;
  getVisitCardById(cardInformation.id).replaceWith(
    createVisitCard(cardInformation).rendered
  );
  CustomEvents.sendEvent(CustomEvents.eventNames.reloadCards);
});

document.addEventListener(CustomEvents.eventNames.reloadCards, () => {
  allVisibleVisitCards().length > 0
    ? noCardsMessage().classList.add("hidden")
    : noCardsMessage().classList.remove("hidden");
});
