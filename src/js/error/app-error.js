import { ErrorMessage } from "../html-element/show-error-message.js";

export const errorMessages = {
  loginError: "Login Error",
  fetchError: "Fetch Error",
  wrongToken: "Wrong token",
  wrongLoginCredentials: "Wrong Login or password",
  cardNotExists: "Card Not Exists",
  connectionError: "Connection Error",
  visitStatusError: "Visit Check Information",
};

export class AppError extends Error {
  constructor(message, context = { rawError: undefined, context: {} }) {
    super(message);
    this.name = "AppError";
    this.rawError = context.rawError;
    this.context = { ...context.rawError?.context, ...context.context };

    this.stack = context.rawError?.stack;
  }
}
export const handleError = (error) => {
  console.log(error.message);
  error.stack && console.log(error.stack);
  Object.keys(error.context).length &&
    console.log(`Context: \n ${JSON.stringify(error.context, null, 2)}`);
  new ErrorMessage(error.message);
};
export const catchAsyncError = (asyncErrorEvent) => {
  if (
    asyncErrorEvent instanceof PromiseRejectionEvent &&
    asyncErrorEvent.reason.name === "AppError"
  ) {
    asyncErrorEvent.preventDefault();
    const error = asyncErrorEvent.reason;
    handleError(
      new AppError(error.message, {
        rawError: error,
        context: error?.context,
      })
    );
  }
};
export const checkConnectionError = (error) => {
  if (error instanceof TypeError) {
    throw new AppError(errorMessages.connectionError, { rawError: error });
  }
};
export const checkCardNotExistError = (cardId, error) => {
  if (error instanceof AppError && error.context.status === 404) {
    throw new AppError(errorMessages.cardNotExists, {
      rawError: error,
      context: { cardId },
    });
  }
  // throw error;
};
export const checkLoginError = (error) => {
  if (error instanceof AppError && error.context.status === 401) {
    throw new AppError(errorMessages.wrongLoginCredentials, {
      rawError: error,
    });
  }
};
export const checkTokenError = (error) => {
  if (error instanceof AppError && error.context.status === 401) {
    throw new AppError(errorMessages.wrongToken, {
      rawError: error,
    });
  }
};
export const registerErrorEventsListeners = () => {
  window.addEventListener("unhandledrejection", (event) => {
    catchAsyncError(event);
  });
};
