export const CustomEvents = {
  eventNames: {
    closeModalWindow: "close-modal-window",
    sendError: "send-error",
    userLoggedInStatusChanged: "user-logged-in-status-changed",
    logout: "logout",
    login: "login",
    reloadCards: "reload-cards",
    deleteCard: "delete-card",
    updateCard: "update-card",
    addCard: "add-card",
  },
  eventOptions: { bubbles: true, cancelable: true },
  getEvent: function (eventName, context) {
    return new CustomEvent(eventName, {
      ...this.eventOptions,
      detail: { ...context },
    });
  },
  sendEvent: function (eventName, context) {
    const element = this.rendered ?? document;
    element.dispatchEvent(this.getEvent(eventName, context), {
      detail: { ...context },
    });
  },
};
