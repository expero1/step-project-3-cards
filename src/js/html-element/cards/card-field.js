import { CustomHTMLEement } from "../custom-html-element.js";
import { context } from "./field-values.js";

export class CardField extends CustomHTMLEement {
  constructor(fieldKey, visitCardInformation) {
    super({ fieldKey, visitCardInformation });

    this.html = `<li class = "visit-card__field" data-field-name=${this.props.fieldKey}><b>${context[fieldKey]}: </b>${this.props.visitCardInformation[fieldKey]}</li>`;
  }
}
