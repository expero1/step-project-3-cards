import { doctors } from "../../constants.js";
import { CardField } from "./card-field.js";
import { VisitCard } from "./visit-card.js";

export class CardiologistCard extends VisitCard {
  constructor(cardiologistVisitCard) {
    super(cardiologistVisitCard);
    for (let propertie of [
      "patientArterialPressure",
      "patientBodyMassIndex",
      "patientPastIllness",
      "patientAge",
    ]) {
      this.addElementToTree(
        this.stringToHtmlElement(new CardField(propertie, this.props).html),
        this.insertContentPlace,
        "beforeend"
      );
    }
    this.shortCardInformation();
  }
}
