import { CustomHTMLEement } from "../custom-html-element.js";
import { createVisitCard } from "./create-visit-card.js";
import { NoCardsMessage } from "./no-card-message.js";

export class VisitCardsSection extends CustomHTMLEement {
  constructor(visitCards = []) {
    super({ visitCards });
    this.html = `<div></div>`;
    const noCardsMessage = new NoCardsMessage();
    noCardsMessage.rendered.classList.add("hidden");
    this.addElementToTree(noCardsMessage, this, "beforeend");
    visitCards.forEach((visitCard) => {
      this.addElementToTree(createVisitCard(visitCard));
    });
  }
}
