import { doctors } from "../../constants.js";
import { AppError } from "../../error/app-error.js";
import { CardiologistCard } from "./cardiologist-card.js";
import { DentistCard } from "./dentis-card.js";
import { TherapistCard } from "./therapist-card.js";
import { VisitCardCommon } from "./visit-card-common.js";

export const createVisitCard = (visitCard) => {
  switch (visitCard.doctor) {
    case doctors.cardiologist:
      return new CardiologistCard(visitCard);
    case doctors.dentist:
      return new DentistCard(visitCard);
    case doctors.therapist:
      return new TherapistCard(visitCard);
    default:
      // return new VisitCard(visitCard);
      console.log("unknown card type");
      return new CardiologistCard(visitCard);
  }
};
