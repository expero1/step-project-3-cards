import { doctors } from "../../constants.js";
import { CardField } from "./card-field.js";
import { VisitCard } from "./visit-card.js";

export class DentistCard extends VisitCard {
  constructor(visitDentistCard) {
    super(visitDentistCard);
    const html = `
	${new CardField("lastVisitDate", this.props).html}`;
    this.addElementToTree(
      this.stringToHtmlElement(html),
      this.insertContentPlace,
      "beforeend"
    );
    this.shortCardInformation();
  }
}
