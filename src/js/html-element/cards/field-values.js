export const context = {
  patientName: "Patient Name",
  visitPurpose: "Visit Purpose",
  visitDescription: "Visit Description",
  visitUrgency: "Visit Urgency",
  doctor: "Doctor",
  therapist: "Therapist",
  dentist: "Dentist",
  cardiologist: "Cardiologist",
  patientArterialPressure: "Arterial Pressure",
  patientBodyMassIndex: "Body Mass Index",
  patientPastIllness: "Body Mass Index",
  patientAge: "Age",
  lastVisitDate: "Last Visit Date",
  visitStatus: "Status"
};
