import { CustomHTMLEement } from "../custom-html-element.js";

export class NoCardsMessage extends CustomHTMLEement {
  constructor() {
    super();
    this.html = `<p class="info no-visits">No items have been added</p>`;
  }
}
