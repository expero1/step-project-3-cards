import { CardField } from "./card-field.js";
import { VisitCard } from "./visit-card.js";

export class TherapistCard extends VisitCard {
  constructor(therapistVisitCard) {
    super(therapistVisitCard);
    const html = `${new CardField("patientAge", this.props).html}`;
    this.addElementToTree(
      this.stringToHtmlElement(html),
      this.insertContentPlace,
      "beforeend"
    );
    this.shortCardInformation();
  }
}
