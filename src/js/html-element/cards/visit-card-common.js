import { CustomHTMLEement } from "../custom-html-element.js";
import { CardField } from "./card-field.js";

export class VisitCardCommon extends CustomHTMLEement {
  constructor(visitCardInformation, fieldNames) {
    super(visitCardInformation);

    let html = "";
    fieldNames.forEach((fieldName) => {
      html += new CardField(fieldName, this.props).html;
    });
    this.html = html;
  }
}
