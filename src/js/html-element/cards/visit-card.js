import { CustomHTMLEement } from "../custom-html-element.js";
import { deleteVisit } from "../../controller/controller.js";
import { context } from "./field-values.js";
import { CardField } from "./card-field.js";
import { VisitCardCommon } from "./visit-card-common.js";
import { CreateVisitModal } from "../modals/create-visit-modal.js";
import { CustomEvents } from "../../events/events.js";
export class VisitCard extends CustomHTMLEement {
  constructor(props) {
    super(props);
    this.state = [];
    this.insertContentPlace = ".visit-card__content";
    this.shortCardInformationButtonText = "Show More";
    this.fullCardInformationButtonText = "Show Less";
    this.visitPropertiesOnFirstScreen = ["patientName", "doctor"];
    this.commonFields = [
      "patientName",
      "doctor",
      "visitDescription",
      "visitPurpose",
      "visitUrgency",
      "visitStatus",
    ];
    this.html = `
    <div class="visit-card" data-id="${this.props.id}">
    <button class="btn btn-close visit-card__delete-btn">X</button>
    <ul class="visit-card__content">
      ${new VisitCardCommon(this.props, this.commonFields).html}
    </ul>
      <button class="btn visit-card__show-more-btn" data-status="${
        this.shortCardInformationButtonText
      }">${this.shortCardInformationButtonText}</button>
      <button class="btn visit-card__edit-btn">Edit Visit Information</button>
    </div>
		`;

    this.registerEventListener(
      "click",
      ".visit-card__delete-btn",
      this.deleteVisitCard
    );
    this.registerEventListener(
      "click",
      ".visit-card__edit-btn",
      this.editVisit
    );
    this.registerEventListener(
      "click",
      ".visit-card__show-more-btn",
      this.toggleCardView
    );
  }

  deleteVisitCard = async (event) => {
    event.target.disabled = true;
    try {
      await deleteVisit(this.props.id);
      this.remove();
      CustomEvents.sendEvent(CustomEvents.eventNames.reloadCards);
      this.sendEvent(this.eventNames.deleteCard);
    } finally {
      event.target.disabled = false;
    }
  };
  editVisit = () => {
    const visitModal = new CreateVisitModal({ ...this.props, edit: true });
    document.body.append(visitModal.rendered);
  };
  toggleCardView = (event) => {
    if (event.target.dataset.status === this.shortCardInformationButtonText) {
      this.fullCardInformation(event);
      event.target.innerHTML = this.fullCardInformationButtonText;
      event.target.dataset.status = this.fullCardInformationButtonText;
    } else {
      this.shortCardInformation(event);
      event.target.dataset.status = this.shortCardInformationButtonText;
      event.target.innerHTML = this.shortCardInformationButtonText;
    }
  };
  fullCardInformation = (event) => {
    this.rendered.querySelectorAll(".visit-card__field").forEach((field) => {
      field.classList.remove("hidden");
    });
  };
  shortCardInformation = (event) => {
    this.rendered.querySelectorAll(".visit-card__field").forEach((field) => {
      if (!this.visitPropertiesOnFirstScreen.includes(field.dataset.fieldName))
        field.classList.add("hidden");
    });
  };
}
