import { CustomEvents } from "../events/events.js";
export class CustomHTMLEement {
  /**
   *
   */
  insertContentPlace = undefined; // inner elements selector to place child Elements to
  constructor(props) {
    this.props = props;
    Object.assign(this, CustomEvents);
  }

  set html(string) {
    this._html = string;
    this._htmlElement = this.stringToHtmlElement(string);
  }
  get html() {
    return this._html;
  }

  get rendered() {
    return this._htmlElement;
  }
  set rendered(htmlElement) {
    this._htmlElement = htmlElement;
  }
  getChildElement(selector) {
    return this.rendered.querySelector(selector);
  }

  registerEventListener(eventAction, selectorOrElement, callback) {
    const element =
      selectorOrElement instanceof CustomHTMLEement
        ? selectorOrElement.rendered
        : selectorOrElement instanceof HTMLElement
        ? selectorOrElement
        : this.getChildElement(selectorOrElement);
    element.addEventListener(eventAction, callback.bind(this));
  }

  remove() {
    this.rendered.remove();
  }
  show(selector = "body", where = "afterbegin") {
    const parentElement = document.querySelector(selector);
    parentElement.insertAdjacentElement(where, this.rendered);
  }
  addElementToTree(
    element,
    elementToPlaceIn = this.insertContentPlace || this,
    where = "afterbegin"
  ) {
    const htmlElement =
      element instanceof CustomHTMLEement
        ? element.rendered
        : element instanceof HTMLElement
        ? element
        : this.stringToHtmlElement(element);
    const parentElement =
      elementToPlaceIn === this
        ? this.rendered
        : this.getChildElement(elementToPlaceIn);
    parentElement.insertAdjacentElement(where, htmlElement);
  }
  stringToHtmlElement(string) {
    const parent = document.createElement("div");
    parent.innerHTML = string;
    return parent.children[0];
  }
}
