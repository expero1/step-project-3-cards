import { status, urgency } from "../../constants.js";
import { getVisits } from "../../controller/controller.js";
import { filterVisits as filterController } from "../../controller/filter-visits.js";
import { CustomEvents } from "../../events/events.js";
import { FormElement } from "../../html-element/form-element.js";

export class FilterSection extends FormElement {
  constructor() {
    super();
    this.html = `
		<form id="filter-form">
      <span class="filter-parts">Search:</span>
      <label><input id="searchByKeyword" type="text" name="contentSearch" value=""></label>
      <span class="filter-parts">Status:</span>
      <label><input type="checkbox" name = "visitStatus" value="${status.open}">${status.open}</label>
      <label><input type="checkbox"  name = "visitStatus" value="${status.done}">${status.done}</label>
      <span class="filter-parts">Priority:</span>
      <label><input type="checkbox"  name = "visitUrgency" value="${urgency.urgent}">${urgency.urgent}</label>
      <label><input type="checkbox"  name = "visitUrgency" value="${urgency.important}">${urgency.important}</label>
      <label><input type="checkbox"  name = "visitUrgency" value="${urgency.common}">${urgency.common}</label>
      <input class="reset-form" type="reset" value="Reset Filter">
		</form>
		`;
    this.registerEventListener("input", this, this.filterVisits);

    this.registerEventListener(
      "click",
      ".reset-form",

      (event) => {
        this.rendered.reset();
        this.filterVisits(event);
      }
    );
  }

  filterVisits = async (event) => {
    const formData = this.formToObject();
    const visits = await getVisits();
    const filteredCardsId = filterController(visits, formData).map(
      (visitCard) => visitCard.id
    );
    const visitCardsSection = document.querySelector(".visit-cards-section");
    const visitCards = visitCardsSection.querySelectorAll(".visit-card");
    visitCards.forEach((visitCard) => {
      filteredCardsId.includes(Number(visitCard.dataset.id))
        ? visitCard.classList.remove("hidden")
        : visitCard.classList.add("hidden");
    });
    CustomEvents.sendEvent(CustomEvents.eventNames.reloadCards);
  };
}
