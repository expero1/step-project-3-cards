import { CustomHTMLEement } from "./custom-html-element.js";
import { ShowFormError } from "./show-form-error.js";

export class FormElement extends CustomHTMLEement {
  formToObject() {
    const returnedObject = {};
    const formData = new FormData(this.rendered);

    for (let [key, value] of formData.entries()) {
      let keyIsArray = key.match(/(.*)\[\]$/);
      if (keyIsArray) {
        key = keyIsArray[1];
        !(key in returnedObject) && (returnedObject[key] = []);
        returnedObject[key].push(value);
      } else {
        if (key in returnedObject) {
          if (!Array.isArray(returnedObject[key])) {
            returnedObject[key] = [returnedObject[key]];
          }
          returnedObject[key].push(value);
        } else {
          returnedObject[key] = value;
        }
      }
    }

    return returnedObject;
    // return Object.fromEntries(new FormData(this.rendered));
  }
  addErrorMessage(message, inputName) {
    this.addElementToTree(
      new ShowFormError(message),
      this.getChildElement(`[name=${inputName}]`),
      "afterend"
    );
  }
}
