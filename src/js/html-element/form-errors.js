import { CustomHTMLEement } from "./custom-html-element.js";

export class FormError extends CustomHTMLEement {
  constructor(...errorMessages) {
    super(errorMessages);
    const errorMessagesHTML = this.props.map(
      (errorMessage) => `<li class="form__error-message">${errorMessage}</li>`
    );
    this.html = `<ul class="form__error-messages">${errorMessagesHTML.join(
      ""
    )}</ul>`;
  }
}
