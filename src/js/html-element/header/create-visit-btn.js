import { CustomHTMLEement } from "../custom-html-element.js";
import { CreateVisitModal } from "../modals/create-visit-modal.js";

export class CreateVisitBtn extends CustomHTMLEement {
    constructor(props){
        super(props);

        this.html = `<button class="header__btn create">Create Visit</button>`;

        this.registerEventListener('click', this, function(){
            const visitModal = new CreateVisitModal();
            document.body.append(visitModal.rendered);
        })
    }
}