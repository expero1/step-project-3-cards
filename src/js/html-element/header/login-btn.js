import { CustomHTMLEement } from "../custom-html-element.js";
import { LoginModal } from "../modals/login-modal.js";

export class LoginBtn extends CustomHTMLEement {
    constructor(props){
        super(props);

        this.html = `<button class="header__btn login">Log in</button>`;
        this.registerEventListener('click', this, function(){
            const loginModal = new LoginModal();
            document.body.append(loginModal.rendered);
        })
    }
}