import { CustomHTMLEement } from "../custom-html-element.js";

export class LogoutBtn extends CustomHTMLEement {
    constructor(props){
        super(props);

        this.html = `<button class="header__btn" id="logoutBtn">Log out</button>`;
    }
}