import { CustomHTMLEement } from "./custom-html-element.js";

export class Loader extends CustomHTMLEement {
  constructor(className = "") {
    super({ className });
    this.html = `<div class="loader ${this.props.className}"></div>`;
  }
}
