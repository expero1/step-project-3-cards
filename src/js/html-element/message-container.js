import { CustomHTMLEement } from "./custom-html-element.js";

export class MessageContainer extends CustomHTMLEement {
  constructor() {
    super();
    this.className = ".message-container";
    this.insertContentPlace = ".message-container__inner";
    const elementOnPage = document.getElementsByClassName(this.className)[0];
    if (!elementOnPage) {
      this.html = `<div class="message-container"><div class="message-container__inner"></div></div>`;
      this.show();
    } else this.rendered = elementOnPage;
  }
}
