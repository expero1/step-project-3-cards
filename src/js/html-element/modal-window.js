import { CustomHTMLEement } from "./custom-html-element.js";

export class ModalWindow extends CustomHTMLEement {
  constructor(contentElement) {
    super(contentElement);
    this.html = `<div class="modal-window"><div class="modal-window__inner">
	<button class="close-window">Close Window</button>
	</div></div>`;

    this.addElementToTree(contentElement, ".modal-window__inner");
    this.registerEventListener("click", ".close-window", this.closeWindow);
    this.registerEventListener(
      this.eventNames.closeModalWindow,
      this,
      this.closeWindow
    );
  }
  closeWindow() {
    document.body.classList.remove("modal-open");
    this.remove();
  }
  showWindow() {
    document.body.classList.add("modal-open");
    super.show();
  }
}
