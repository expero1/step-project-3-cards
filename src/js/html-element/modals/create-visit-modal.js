import { doctors, status, urgency } from "../../constants.js";
import { updateVisit } from "../../controller/controller.js";
import { setVisit } from "../../controller/controller.js";
import { CustomEvents } from "../../events/events.js";
import { CustomHTMLEement } from "../custom-html-element.js";

export class CreateVisitModal extends CustomHTMLEement {
  constructor(props) {
    super(props);

    this.props = this.props || {};

    this.modalTitle = this.props.edit ? "Edit visit ✎" : "Create visit ✎";
    this.submitBtnValue = this.props.edit ? "Edit visit" : "Create new visit";

    this.html = `<div>
        <div id="overlay"></div>
        <div class="newVisit">
            <div class="newVisit__container">
            <h2 class="newVisit__title">${this.modalTitle}</h2>
            <form class="newVisitForm" action="#">
                <select required class="selectDoctor" name="doctor">
                    <option ${
                      !this.props.doctor ? "selected" : ""
                    } disabled value="">Choose a doctor</option>
                    <option ${
                      this.props.doctor === doctors.cardiologist
                        ? "selected"
                        : ""
                    } value="${doctors.cardiologist}">${
      doctors.cardiologist
    }</option>
                    <option ${
                      this.props.doctor === doctors.dentist ? "selected" : ""
                    } value="${doctors.dentist}">${doctors.dentist}</option>
                    <option ${
                      this.props.doctor === doctors.therapist ? "selected" : ""
                    } value="${doctors.therapist}">${doctors.therapist}</option>
                </select>
                <div class="all-doctors">
                    <div class="purpose">
                        <label class="form-label" for="visit-purpose">Purpose of the visit:</label>
                        <input type="text" id="visit-purpose" name="visitPurpose" placeholder="Purpose of the visit" value="${
                          this.props.visitPurpose || ""
                        }" required>
                    </div>
                    <div class="urgency">
                        <label class="form-label" for="visitUrgency">Choose the urgency:</label>
                        <select class="form-select" name="visitUrgency" required>
                            <option ${
                              this.props.visitUrgency === urgency.urgent
                                ? "selected"
                                : ""
                            } value="${urgency.urgent}">${
      urgency.urgent
    }</option>
                            <option ${
                              this.props.visitUrgency === urgency.important
                                ? "selected"
                                : ""
                            } value="${urgency.important}">${
      urgency.important
    }</option>
                            <option ${
                              this.props.visitUrgency === urgency.common
                                ? "selected"
                                : ""
                            } value="${urgency.common}">${
      urgency.common
    }</option>
                        </select>
                    </div>
                    <div class="status">
                        <label class="form-label" for="visitStatus">Select visit status:</label>
                        <select class="form-select" name="visitStatus" required>
                            <option ${
                              this.props.visitStatus === "${status.open}"
                                ? "selected"
                                : ""
                            } value="${status.open}">${status.open}</option>
                            <option ${
                              this.props.visitStatus === "${status.done}"
                                ? "selected"
                                : ""
                            } value="${status.done}">${status.done}</option>
                        </select>
                    </div>
                    
                    <div class="description">
                        <label class="form-label" for="visit-description">Description of the visit:</label>
                        <textarea id="visit-description" required name="visitDescription" placeholder="Description of the visit..." style="height: 100px; width: 350px;">${
                          this.props.visitDescription || ""
                        }</textarea>  
                    </div>
                    <div class="user-info">
                        <label class="form-label" for="userName">Name and Last Name:</label>
                        <input type="text" id="userName" name="patientName" placeholder="Name and Last Name" value="${
                          this.props.patientName || ""
                        }" required>
                    </div>
                    <div class="age field__patientAge">
                        <label class="form-label" for="user-age">Age:</label>
                        <input type="number" id="user-age" name="patientAge" value="${
                          this.props.patientAge || ""
                        }" min="1" max="100">
                    </div>
                    <div class="visit field__lastVisitDate">
                        <label class="form-label" for="last-visit">Last visit date:</label>
                        <input type="date" id="last-visit" name="lastVisitDate" value="${
                          this.props.lastVisitDate || ""
                        }">
                    </div>
                    <div class="pressure field__patientArterialPressure">
                        <label class="form-label" for="arterial-pressure">Arterial pressure:</label>
                        <input type="number" id="arterial-pressure" name="patientArterialPressure" value="${
                          this.props.patientArterialPressure || ""
                        }" min="60" max="200">
                    </div>
                    <div class="mass-index field__patientBodyMassIndex">
                        <label class="form-label" for="mass-index">Body Mass Index:</label>
                        <input type="number" id="mass-index" name="patientBodyMassIndex" value="${
                          this.props.patientBodyMassIndex || ""
                        }" min="18" max="55">
                    </div>
                    <div class="illness field__patientPastIllness">
                        <label class="form-label" for="illness">Past Illness:</label>
                        <input type="text" id="illness" placeholder="Past Illness" name="patientPastIllness" value="${
                          this.props.patientPastIllness || ""
                        }">
                    </div>
                    </div>
                    <div class="newVisit__control-btns">
                        <button class="newVisit__create">${
                          this.submitBtnValue
                        }</button> 
                        <button class="newVisit__cancel">Cancel</button> 
                    </div>
                    <button class="newVisit__close-btn">×</button>
                </form>
            </div>
        </div>
        
        </div>`;
    const submitButton = this.getChildElement(".newVisit__create");
    this.changeDynamicFieldsVisibility();

    this.registerEventListener("click", "#overlay", this.remove);
    this.registerEventListener(
      "change",
      '[name="doctor"]',
      this.changeDynamicFieldsVisibility
    );
    this.registerEventListener("click", ".newVisit__cancel", this.remove);
    this.registerEventListener("click", ".newVisit__close-btn", this.remove);
    this.registerEventListener(
      "submit",
      ".newVisitForm",
      async function (event) {
        event.preventDefault();
        const formData = new FormData(this.getChildElement(".newVisitForm"));
        submitButton.disabled = true;
        let saveFormTask;
        if (this.props.edit) {
          saveFormTask = updateVisit(
            this.props.id,
            Object.fromEntries(formData)
          );
        } else {
          saveFormTask = setVisit(Object.fromEntries(formData));
        }
        try {
          await saveFormTask;
          this.remove();
        } finally {
          submitButton.disabled = false;
        }
      }
    );
  }

  changeDynamicFieldsVisibility(event) {
    // get doctor input value
    const doctorValue = event
      ? event.target.value
      : this.getChildElement('[name="doctor"]').value;
    // hide dynamic fields
    const dynamicFields = [
      { name: "patientAge", required: true },
      { name: "lastVisitDate", required: true },
      { name: "patientArterialPressure", required: true },
      { name: "patientBodyMassIndex", required: true },
      { name: "patientPastIllness", required: true },
    ];

    dynamicFields.forEach((field) => {
      try {
        this.getChildElement(`.field__${field.name}`).style.display = "none";
        this.getChildElement(
          `.field__${field.name} > input, .field__${field.name} > select`
        ).required = false;
      } catch (e) {
        // ...
      }
    });
    const showFieldNames = [];
    // swtich case show required fields
    switch (doctorValue) {
      case doctors.cardiologist:
        showFieldNames.push("patientAge");
        showFieldNames.push("patientArterialPressure");
        showFieldNames.push("patientBodyMassIndex");
        showFieldNames.push("patientPastIllness");
        break;
      case doctors.therapist:
        showFieldNames.push("patientAge");
        break;
      case doctors.dentist:
        showFieldNames.push("lastVisitDate");
        break;
    }
    showFieldNames.forEach((fieldName) => {
      try {
        this.getChildElement(`.field__${fieldName}`).style.display = "block";
        let dynamicField = dynamicFields.find((field) => {
          return (field.name = fieldName);
        });

        if (dynamicField.required) {
          this.getChildElement(
            `.field__${fieldName} > input, .field__${fieldName} > select`
          ).required = true;
        }
      } catch (e) {
        // ...
      }
    });
  }
}
