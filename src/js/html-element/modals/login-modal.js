import { login } from "../../controller/controller.js";
import { handleError } from "../../error/app-error.js";
import { CustomHTMLEement } from "../custom-html-element.js";

export class LoginModal extends CustomHTMLEement {
  constructor(props) {
    super(props);

    this.html = `<div>
            <div id="overlay"></div>
                <div class="modal-login"> 
                    <form class="login-form" method="post">
                        <label for="email">User Email:</label>
                        <input type="email" name="email" id="email" placeholder="Email Address" required>
                        <label for="password">User Password:</label>
                        <input type="password" name="password" id="password" placeholder="Password" required>
                        <button name="login" class="submit-login" type="submit">Log in</button>
                        <button name="cancel" class="cancel-login">Cancel</button>
                    </form>  
                </div>
                </div>`;
    const submitButton = this.getChildElement(".submit-login");
    this.registerEventListener("click", "#overlay", this.remove);
    this.registerEventListener("click", ".cancel-login", this.remove);
    this.registerEventListener("submit", ".login-form", async function (event) {
      event.preventDefault();
      const formData = new FormData(this.getChildElement(".login-form"));
      submitButton.disabled = true;
      try {
        await login(formData.get("email"), formData.get("password"));
      } finally {
        submitButton.disabled = false;
      }
      this.remove();
    });
  }
}
