import { isLoggedIn, logout } from "../../controller/controller.js";
import { CustomHTMLEement } from "../custom-html-element.js";
import { CreateVisitBtn } from "../header/create-visit-btn.js";
import { LoginBtn } from "../header/login-btn.js";
import { LogoutBtn } from "../header/logout-btn.js";

export class Header extends CustomHTMLEement {
  constructor(props) {
    super(props);

    this.insertContentPlace = ".header__actions";
    this.html = `<header class="header">
            <div class="header__logo">
                <a href="#"><img class="img" src="img/logo.png" alt="logo" width="100px"></a>
            </div>
            <div class="header__actions"></div>
        </header>`;

    this.renderHeaderActions();

    document.addEventListener(
      "user-logged-in-status-changed",
      this.renderHeaderActions.bind(this)
    );
  }

  renderHeaderActions() {
    this.getChildElement(this.insertContentPlace).innerHTML = "";
    if (isLoggedIn()) {
      const btnCreateVisit = new CreateVisitBtn();
      const logoutBtn = new LogoutBtn();

      this.addElementToTree(btnCreateVisit.rendered);
      this.addElementToTree(
        logoutBtn.rendered,
        this.insertContentPlace,
        "beforeend"
      );

      this.registerEventListener("click", "#logoutBtn", logout);
    } else {
      const btnLogin = new LoginBtn();
      this.addElementToTree(btnLogin.rendered);
    }
  }
}
