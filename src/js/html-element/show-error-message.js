import { MESSAGE_TYPE, ShowMessage } from "./show-message.js";

export class ErrorMessage extends ShowMessage {
  constructor(error) {
    super(error, MESSAGE_TYPE.ERROR);
  }
}
