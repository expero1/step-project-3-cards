import { CustomHTMLEement } from "./custom-html-element.js";
import { MessageContainer } from "./message-container.js";
export const MESSAGE_TYPE = {
  ERROR: "error",
  INFO: "info",
};
export class ShowMessage extends CustomHTMLEement {
  constructor(message, message_type = MESSAGE_TYPE.INFO) {
    super({ message, message_type });
    this.html = `
	<div class="message-window ${message_type}">
	<div class="message-content">${message}</div>
	<button class="close-message">Close</div>
	</div>`;
    const messageContainer = new MessageContainer();
    messageContainer.addElementToTree(this);
    setTimeout(() => {
      this.remove();
    }, 5000);
    this.registerEventListener("click", ".close-message", () => {
      this.remove();
    });
  }
}
