import {
  AppError,
  errorMessages,
  checkConnectionError,
} from "../error/app-error.js";

const API = {
  login: "https://ajax.test-danit.com/api/v2/cards/login",
  cards: "https://ajax.test-danit.com/api/v2/cards",
  card: (cardId) => `https://ajax.test-danit.com/api/v2/cards/${cardId}`,
};
const requestMethods = {
  GET: "GET",
  POST: "POST",
  PUT: "PUT",
  DELETE: "DELETE",
};

const fetchData = async (url, options) => {
  let requestOptions = {
    ...options,
    headers: {
      "Content-Type": "application/json",
      ...options.headers,
    },
  };
  const response = await fetch(url, {
    ...requestOptions,
  }).catch((error) => {
    checkConnectionError(error);
    throw error;
  });
  if (response.ok) return response;
  throw new AppError(errorMessages.fetchError, {
    context: {
      status: response.status,
      body: await response.text(),
      response,
      url,
    },
  });
};

const fetchJSON = async (
  url,
  options = { method: requestMethods.GET, body: undefined, headers: {} }
) => {
  const response = await fetchData(url, options);
  return await response.json();
};
const fetchResponse = async (
  url,
  options = { method: requestMethods.GET, body: undefined, headers: {} }
) => {
  const response = await fetchData(url, options);
  return response.text();
};

export const getToken = async (email, password) => {
  const options = {
    method: requestMethods.POST,
    body: JSON.stringify({ email, password }),
  };
  const response = await fetchResponse(API.login, options);

  return response;
};
const withToken = (token) => {
  return { headers: { Authorization: `Bearer ${token}` } };
};
export const getVisits = (token) => {
  return fetchJSON(API.cards, {
    ...withToken(token),
  });
};
export const setVisit = (token, cardInfo) => {
  const method = requestMethods.POST;
  const body = JSON.stringify(cardInfo);
  return fetchJSON(API.cards, { method, body, ...withToken(token) });
};
export const updateVisit = (token, cardId, cardInfo) => {
  const method = requestMethods.PUT;
  const body = JSON.stringify(cardInfo);
  return fetchJSON(API.card(cardId), {
    method,
    body,
    ...withToken(token),
  });
};
export const deleteVisit = async (token, cardId) => {
  const method = requestMethods.DELETE;
  return fetchResponse(API.card(cardId), { method, ...withToken(token) });
};
