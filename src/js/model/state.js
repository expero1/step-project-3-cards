import { AppError } from "../error/app-error.js";

const ACTIONS = {
  GET_VISITS: "get-visits",
  SET_VISITS: "set-visits",
  GET_VISIT: "get-visit",
  SET_VISIT: "set-visit",
  UPDATE_VISIT: "update-visit",
  DELETE_VISIT: "delete-visit",
  SET_ASK_VISITS: "set-ask-visits",
  GET_ASK_VISITS: "get-ask-visits",
  CLEAR_STATE: "clear-state",
  SAVE_FORM_TO_STATE: "save-form-to-state",
  GET_FORM_FROM_STATE: "get-form-from-state",
  CLEAR_FORM_IN_STATE: "clear-form-in-state",
  SAVE_FORM_ERRORS_TO_STATE: "save-form-errors-to-state",
  GET_FORM_ERRORS_FROM_STATE: "get-form-errors-from-state",
  CLEAR_FORM_ERRORS_IN_STATE: "clear-form-errors-in-state",
};
const _state = {
  visits: [],
  isAskVisits: false,
  currentForm: {},
  currentFormErrors: {},
};
window.STATE = _state;
export const State = {
  setToken: function (token) {
    localStorage.setItem("token", token);
  },
  getToken: function () {
    return localStorage.getItem("token");
  },
  removeToken: function () {
    localStorage.removeItem("token");
  },
  clearState: function () {
    this.reducer(ACTIONS.CLEAR_STATE);
  },
  getVisits: function () {
    return this.reducer(ACTIONS.GET_VISITS);
  },
  setVisits: function (visitsInformation) {
    this.reducer(ACTIONS.SET_VISITS, { visitsInformation });
  },
  setVisit: function (id, visitInformation) {
    this.reducer(ACTIONS.SET_VISIT, { id, visitInformation });
  },
  updateVisit: function (id, visitInformation) {
    this.reducer(ACTIONS.UPDATE_VISIT, { id, visitInformation });
  },
  getVisit: function (id) {
    return this.reducer(ACTIONS.GET_VISIT, { id });
  },
  deleteVisit: function (id) {
    return this.reducer(ACTIONS.DELETE_VISIT, { id });
  },
  setAskVisits: function () {
    this.reducer(ACTIONS.SET_ASK_VISITS, { askVisits: true });
  },
  isAskVisits: function () {
    return this.reducer(ACTIONS.GET_ASK_VISITS);
  },
  isLoggenIn: function () {
    return this.getToken() ? true : false;
  },

  saveFormToState: function (formData) {
    this.reducer(ACTIONS.SAVE_FORM_TO_STATE, { formData });
  },
  getFormFromState: function () {
    return this.reducer(ACTIONS.GET_FORM_FROM_STATE);
  },
  clearFormInState: function () {
    this.reducer(ACTIONS.CLEAR_FORM_IN_STATE);
  },
  saveFormErrorsToState: function (formData) {
    this.reducer(ACTIONS.SAVE_FORM_ERRORS_TO_STATE, { formData });
  },
  getFormErrorsFromState: function () {
    return this.reducer(ACTIONS.GET_FORM_ERRORS_FROM_STATE);
  },
  clearFormErrorsInState: function () {
    this.reducer(ACTIONS.CLEAR_FORM_ERRORS_IN_STATE);
  },

  reducer: function (action, props) {
    const state = _state;
    switch (action) {
      case ACTIONS.GET_VISITS:
        return state.visits;
      case ACTIONS.SET_VISITS:
        state.visits = props.visitsInformation;
        state.isAskVisits = true;
        break;
      case ACTIONS.SET_VISIT:
        state.visits.unshift({ id: props.id, ...props.visitInformation });
        this.setAskVisits();
        break;
      case ACTIONS.GET_VISIT:
        return state.visits.find(({ id: visitId }) => props.id === visitId);
      case ACTIONS.UPDATE_VISIT:
        let visitIndex = state.visits.findIndex(
          ({ id: visitId }) => props.id === visitId
        );
        state.visits[visitIndex] = { ...props.visitInformation, id: props.id };
        return this.getVisit(props.id);
      case ACTIONS.GET_ASK_VISITS:
        return state.isAskVisits;
      case ACTIONS.SET_ASK_VISITS:
        state.isAskVisits = props.askVisits;
        break;
      case ACTIONS.DELETE_VISIT:
        state.visits = state.visits.filter(({ id }) => props.id !== id);
        break;
      case ACTIONS.CLEAR_STATE:
        state.visits = [];
        state.isAskVisits = false;
        break;

      case ACTIONS.SAVE_FORM_TO_STATE:
        state.currentForm = { ...state.currentForm, ...props.formData };
        break;
      case ACTIONS.GET_FORM_FROM_STATE:
        return state.currentForm;
      case ACTIONS.CLEAR_FORM_IN_STATE:
        this.state.currentForm = {};
        break;

      case ACTIONS.SAVE_FORM_ERRORS_TO_STATE:
        state.currentFormErrors = {};
        state.currentFormErrors = { ...props.formData };
        break;
      case ACTIONS.GET_FORM_ERRORS_FROM_STATE:
        return state.currentFormErrors;
      case ACTIONS.CLEAR_FORM_ERRORS_IN_STATE:
        this.state.currentFormErrors = {};

      default:
        throw new AppError("Reducer Error");
    }
  },
};
